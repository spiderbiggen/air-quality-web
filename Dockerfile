FROM node:14-alpine as builder

RUN mkdir -p /opt/air_quality
WORKDIR /opt/air_quality

COPY ./package*.json ./
RUN npm ci
COPY ./tsconfig.json ./
COPY ./.eslintrc.js ./
COPY ./*.config.ts ./
COPY ./*.config.js ./
COPY ./public ./public
COPY ./src ./src

ARG VUE_APP_PUBLIC_PATH
ARG VUE_APP_TITLE
ARG VUE_APP_API_URL

ENV PUBLIC_PATH=${PUBLIC_PATH}
ENV VUE_APP_TITLE=${VUE_APP_TITLE}
ENV VUE_APP_API_URL=${VUE_APP_API_URL}

RUN npm run build

FROM nginx:1-alpine
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /opt/air_quality/dist /var/www/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
