import { Options, Vue } from 'vue-class-component';
import store from '@/store';
import ProfileIcon from '@/components/icons/ProfileIcon.vue';
import { mapGetters } from 'vuex';
import Client from './support/Client';
import { capatalizeFirstLetter } from '@/support/WordFormatter';

@Options({
  components: {
    ProfileIcon,
  },
  computed: {
    ...mapGetters(['user']),
  },
  methods: {
    capatalizeFirstLetter,
  },
})
export default class App extends Vue {
  protected isMobileActive = false;
  protected isProfileActive = false;

  protected isAuthenticated(): boolean {
    return store.getters.token !== null;
  }

  protected toggleMobileMenu(): void {
    this.isMobileActive = !this.isMobileActive;
  }

  protected closeMobileMenu(): void {
    this.isMobileActive = false;
  }

  protected toggleProfile(): void {
    this.isProfileActive = !this.isProfileActive;
  }

  protected closeProfile(): void {
    this.isProfileActive = false;
  }

  protected logOut(): void {
    this.closeMobileMenu();

    Client.logout();
  }
}
