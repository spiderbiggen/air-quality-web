import Vuex from "vuex";
import VuexPersist from "vuex-persist";

const vuexPersist = new VuexPersist({ key: "vuex" });

export default new Vuex.Store({
  state: {
    auth: {
      token: null,
      user: null,
    },
    login: {
      email: '',
      isChecked: false,
    }
  },
  getters: {
    token(state) {
      return state.auth.token;
    },
    user(state) {
      return state.auth.user;
    },
  },
  mutations: {
    UPDATE_TOKEN: (state, payload) => {
      state.auth.token = payload;
    },
    UPDATE_USER: (state, payload) => {
      state.auth.user = payload;
    },
    UPDATE_LOGIN_INFO: (state, payload) => {
      state.login = payload;
    },
  },
  actions: {
    tokenUpdate: (context, payload) => {
      context.commit('UPDATE_TOKEN', payload);
    },
    userUpdate: (context, payload) => {
      context.commit('UPDATE_USER', payload);
    },
    loginInfoUpdate: (context, payload) => {
      context.commit('UPDATE_LOGIN_INFO', payload);
    },
  },
  modules: {},
  plugins: [vuexPersist.plugin],
});
