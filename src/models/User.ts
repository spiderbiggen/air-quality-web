import Model from '@/models/Model';
import { Role } from '@/support/enums/User/Role';
import { HttpMethod } from '@/support/enums/HttpMethod';
import Builder from '@/support/Builder';

export default class User extends Model {
  protected $name = 'Users';
  protected $endpoint = '/users';

  protected $fillable: Array<string> = ['email', 'password', 'role', 'disabled_at'];

  // Attributes
  public id?: string;
  public email?: string;
  public password?: string;
  public role?: Role | string;
  public created_at?: string;
  public updated_at?: string;
  public disabled_at?: Date;

  public _links = { self: '' };

  public static async current(): Promise<User> {
    return await new Builder(new User()).request(HttpMethod.GET, `users/current_user`);
  }
}
