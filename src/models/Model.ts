import Builder from '@/support/Builder';

export default class Model {
  /** Name of the model. */
  protected $name = "Model";

  /** Endpoint. Everything that comes after the base URL. */
  protected $endpoint = "";

  /** Primary key, used for API calls. */
  protected $primaryKey = "id";

  /** List of keys which are sendable to the API. */
  protected $fillable: Array<string> = [];

  /** List of default includes. */
  protected $defaultIncludes: Array<string> = [];

  //------------------------ Helpers ------------------------//

  /** Get the instance name. */
  public getModelName(): string {
    return this.$name;
  }

  /** Get the base endpoint. */
  public getEndpoint(): string {
    return this.$endpoint;
  }

  /** Get the primary key name. */
  public getPrimaryKey(): string {
    return this.$primaryKey;
  }

  /** Get the primary key value of the model. */
  public getPrimaryKeyValue(): string {
    return (this as any)[this.$primaryKey];
  }

  /**
   * Checks the given key against the fillable array.
   * 
   * @param key The key to check.
   */
  public isFillable(key: string): boolean {
    return this.$fillable.includes(key);
  }

  /** Checks if there are any fillables. */
  public hasFillables(): boolean {
    return this.$fillable.length > 0;
  }

  //------------------------ Query functions ------------------------//

  public static query(): Builder {
    return new Builder(new this);
  }

  /**
   * Add filters to the query.
   * 
   * @param filters Filters object or filter key.
   * @param value Filter value.
   */
  public static filter(filters: object | string, value?: any): Builder {
    return new Builder(new this).filter(filters, value);
  }

  /**
   * Add includes to the query.
   * 
   * @param includes Include keys.
   */
  public static include(includes: Array<string> | string): Builder {
    return new Builder(new this).include(includes);
  }

  /**
   * Set the page for the paginated results.
   * 
   * @param page Page number.
   */
  public static page(page: number): Builder {
    return new Builder(new this).page(page);
  }

  /**
   * Set the page limit.
   * 
   * @param limit Page limit.
   */
  public static limit(limit: number): Builder {
    return new Builder(new this).limit(limit);
  }

  //------------------------ Calls ------------------------//

  /** 
   * GET call to the API to retrieve a single object. 
   * 
   * @param id Id to use for the API call.
   */
  public static find(id: string): Promise<any> {
    return new Builder(new this).find(id);
  }

  /** 
   * GET call to the API to retrieve paginated list of objects.
   * 
   * @param parseToModel Whether to parse the results to models.
   */
  public static list(parseToModel = true): Promise<any> {
    return new Builder(new this).list(parseToModel);
  }

  /** 
   * POST call to the API to create a resource. 
   * 
   * @param attributes The data to use for the API call.
   */
  public static create(attributes?: object): Promise<any> {
    return new Builder(new this).create(attributes);
  }

  /** 
   * PATCH call to the API to edit a given resource. 
   * 
   * @param id Id to use for the API call.
   * @param attributes The data to use for the API call.
   */
  public static update(id: string, attributes?: object): Promise<any> {
    return new Builder(new this).update(id, attributes);
  }

  /**
   * PATCH call to the API to edit this resources.
   * 
   * @param attributes The data to use for the API call.
   */
  public update(attributes?: object): Promise<any> {
    return new Builder(this).update("", attributes);
  }

  /** 
   * POST call to the API to edit a given resource. 
   * 
   * @param id Id to use for the API call.
   * @param attributes The data to use for the API call.
   */
  public static image(id: string, attributes?: object): Promise<any> {
    return new Builder(new this).image(id, attributes);
  }

  /** 
   * DELETE call to the API to delete a given resource. 
   * 
   * @param id Id to use for the API call.
   */
  public static delete(id: string): Promise<any> {
    return new Builder(new this).delete(id);
  }

  /**
   * DELETE call to the API to delete this resource.
   */
  public delete(): Promise<any> {
    return new Builder(this).delete(this.getPrimaryKeyValue());
  }
}
