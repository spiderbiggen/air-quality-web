import Model from "@/models/Model";
import Floor from "@/models/Floor";

export default class Building extends Model {
  protected $name = "Building";
  protected $endpoint = "/buildings";

  protected $fillable: Array<string> = ['name'];

  // Attributes
  public id?: string;
  public name?: string;

  public postal_code?: string
  public house_number?: string
  public house_number_addition?: string

  public created_at?: string;
  public updated_at?: string;

  public _links = { self: "", image: ""};
  
  // Includes
  public floors?: Array<Floor> = [];
  public stats: { device_count: number, score: number } | null = null;
}
