import Model from "@/models/Model";
import SensorData from "@/support/interfaces/SensorData";

export default class Device extends Model {
  protected $name = "Device";
  protected $endpoint = "/devices";

  // Attributes
  public id?: string;
  public x_position?: number;
  public y_position?: number;
  public disabled_at?: string;
  public created_at?: string;
  public updated_at?: number;

  public _links = { self: "" };

  // Includes
  public sensor_data: Array<SensorData> = [];
}
