import Building from './Building';
import Model from "@/models/Model";
import Device from "@/models/Device";

export default class Floor extends Model {
  protected $name = "Floors";
  protected $endpoint = "/floors";

  // Attributes
  public id?: string;
  public building_id?: string;
  public level?: number;
  public image?: string;
  public created_at?: string;
  public updated_at?: string;

  public _links = { self: "", image: "" };

  // Includes
  public building?: Building;
  public stats: { device_count: number, score: number } | null = null;
  public devices?: Array<Device>;
}
