export enum Grade {
    None,
    Bad,
    Average,
    Good,
}
