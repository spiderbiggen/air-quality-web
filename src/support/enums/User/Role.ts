export enum Role {
  User = 'User',
  Researcher = 'Researcher',
  Manager = 'Manager',
  Admin = 'Admin',
}
