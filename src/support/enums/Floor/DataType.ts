export enum DataType {
    CO2,
    CO,
    VOC,
    PM2_5,
    Temperature,
    Humidity,
    Battery,
}
