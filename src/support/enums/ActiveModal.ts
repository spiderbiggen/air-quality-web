export enum ActiveModal {
    None = 'none',
    Create = 'create',
    Edit = 'edit',
    Delete = 'delete',
}
