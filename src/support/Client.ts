import Axios, { AxiosError, AxiosResponse } from "axios";
import { HttpMethod } from "@/support/enums/HttpMethod";
import Qs from "qs";
import store from "@/store";
import router from "@/router";

export default class Client {
  /**
   * Makes a request for the given data.
   *
   * @param method HttpMethod to use.
   * @param endpoint Endpoint to call.
   * @param payload Data to send.
   * @param hasMedia Whether the payload contains media.
  */
  public static async request(
    method: HttpMethod,
    endpoint: string,
    payload: object | Array<string>,
    hasMedia = false,
  ) {
    const baseURL = Client.getBaseUrl();
    const headers: { [key: string]: string | boolean } = {
      Accept: 'application/json',
      'Content-type': !hasMedia
        ? 'application/json'
        : 'multipart/form-data',
    };
    if (Client.isAuthenticated()) {
      headers.authorization = store.getters.token;
    }
    const paramsSerializer = (params: object) => Qs.stringify(params, { arrayFormat: 'brackets' });
    const client = Axios.create({ baseURL, headers, paramsSerializer });

    payload = payload ? payload : {};

    if (hasMedia) {
      payload = Client.objectToFormData(payload);
    }

    if (method.toLowerCase() === HttpMethod.GET || method.toLowerCase() === HttpMethod.DELETE) {
      payload = {
        params: payload
      };
    }

    return (client as any)[method](endpoint, payload).then((response: AxiosResponse) => {
      return Promise.resolve(response);
    }).catch((error: AxiosError) => {
      if (error.response?.status == 401 && error.response?.data?.msg == 'Token has expired') {
        Client.logout();
      }
      return Promise.reject(error);
    });
  }

  /**
   * Login call.
   *
   * @param payload Login credentials.
   */
  public static async login(payload: { email: string, password: string }) {
    const baseURL = Client.getBaseUrl();
    const headers = { Accept: "application/json" };
    const client = Axios.create(({ baseURL, headers }));

    return (client as any)[HttpMethod.POST]("users/login", payload).then((response: AxiosResponse) => {
      return Promise.resolve(response);
    }).catch((error: AxiosError) => {
      return Promise.reject(error);
    });
  }

  public static logout(): void {
    store.dispatch("tokenUpdate", null);
    store.dispatch("userUpdate", null);

    router.push({ name: "login" });
  }



  /** Checks whether the token exists in the store. */
  public static isAuthenticated(): boolean {
    return store.getters.token !== null;
  }

  /** Returns base URL. */
  public static getBaseUrl(): string {
    return process.env.VUE_APP_API_URL ?? 'https://localhost:8080'; // Gota move this somewhere
  }

  /**
   * Transforms payload to form data, recursively.
   *
   * @param payload
   * @param form
   * @param namespace
   */
  public static objectToFormData(payload: any, form: any = null, namespace: any = null): FormData {
    const formData: FormData = form || new FormData();
    let formKey: string | null = null;

    Object.keys(payload).forEach((key: string) => {
      formKey = (namespace) ? `${namespace}[${key}]` : key;

      if (typeof payload[key] === 'object' && !(payload[key] instanceof File)) {
        Client.objectToFormData(payload[key], formData, key);
      } else {
        formData.append(formKey, payload[key]);
      }
    });

    return formData;
  }

}
