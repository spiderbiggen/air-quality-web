import { SortOrder } from "@/support/types/SortOrder.ts";

export default interface SortOptions {
    key?: string;
    order?: SortOrder;
}
