export default interface SensorData {
    co2: number | null;
    co: number | null;
    voc: number | null;
    pm2_5: number | null;
    temperature: number | null;
    humidity: number | null;
    created_at: Date | null;
}
