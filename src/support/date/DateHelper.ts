import moment from 'moment';

export const convertDateToDutchFormat = (date: Date): string => {
  return moment(date).format('DD-MM-YYYY');
};

export const convertDateToYearMonthDayFormat = (date: Date): string => {
  return moment(date).format('YYYY-MM-DD');
};

export const isValidDateString = (date: string): boolean => {
  return moment(date).isValid();
};
