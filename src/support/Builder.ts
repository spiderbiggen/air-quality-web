import Model from "@/models/Model";
import SortOptions from "@/support/interfaces/SortOptions";
import { HttpMethod } from "@/support/enums/HttpMethod";
import Client from "@/support/Client";
import { AxiosError } from "axios";

export default class Builder {
    protected model: Model;

    /** Filters to send with the GET call. */
    protected $filters: object = {};

    /** Page number to send with the GET call. */
    protected $page: number | null = null;

    /** Page limit to send with the GET call. */
    protected $limit: number | null = null;

    /** Sort key to send with the GET call. */
    protected $sort: SortOptions = {};

    /** Include keys to send with the GET call. */
    protected $includes: Array<string> = [];

    public constructor(model: Model) {
        this.model = model;
    }

    // --------------- Fluent Functions --------------- //

    public filter(filters: object | string, value?: any): this {
        if (typeof filters === 'string') {
            const key = filters;
            filters = this.$filters;
            (filters as any)[key] = value ? value : null;
            this.$filters = filters;
        }
        this.$filters = filters;

        return this;
    }

    public include(includes: Array<string> | string): this {
        if (typeof includes === "string") {
            this.$includes.push(includes);
            return this;
        }
        this.$includes = includes;

        return this;
    }

    public page(page: number): this {
        this.$page = page;
        
        return this;
    }
    
    public limit(limit: number): this {
        this.$limit = limit;

        return this;
    }

    /**
     * Removes all unfillable attributes from the object.
     * 
     * @param attributes Potential payload to cleanup.
     */
    public getPayload(attributes?: object): object {
        if (attributes === undefined) {
            attributes = { ...{}, ...(this as Record<string, any>) } as Model;
        }

        const keys = Object.keys(attributes);

        if (! this.model.hasFillables()) {
            return attributes;
        }

        keys.forEach((property, index) => {
            if (!this.model.isFillable(property)) delete (attributes as any)[property];
        });
        return attributes;
    }

    // --------------- Fluent Endings --------------- //

    /** 
     * GET call to the API to retrieve a single object. 
     * 
     * @param id Id to use for the API call.
    */
    public find(id: string): Promise<any> {
        return this.request(HttpMethod.GET, `${this.model.getEndpoint()}/${id}`);
    }

    /** 
     * GET call to the API to retrieve paginated list of objects 
     * 
     * @param parseToModel Whether to parse the results to models.
     */
    public list(parseToModel = true): Promise<any> {
        return this.request(HttpMethod.GET, this.model.getEndpoint(), {}, parseToModel);
    }

    /** 
     * POST call to the API to create this resource. 
     * 
     * @param attributes The data to use for the API call
     */
    public create(attributes?: object): Promise<any> {
        return this.request(HttpMethod.POST, this.model.getEndpoint(), this.getPayload(attributes));
    }

    /** 
     * PATCH call to the API to edit a resource. 
     * 
     * @param id Id to use for the API call.
     * @param attributes The data to use for the API call
     */
    public update(id?: string, attributes?: object): Promise<any> {
        return this.request(HttpMethod.PATCH, `${this.model.getEndpoint()}/${id ? id : this.model.getPrimaryKeyValue()}`, this.getPayload(attributes));
    }

    /** 
     * DELETE call to the API to edit a resource. 
     * 
     * @param id Id to use for the API call.
     */
    public delete(id: string): Promise<any> {
        return this.request(HttpMethod.DELETE, `${this.model.getEndpoint()}/${id}`);
    }

    /**
     * POST call to the API to add an image to the resource.
     * 
     * @param id 
     * @param attributes 
     */
    public image(id: string, attributes?: object): Promise<any> {
        return this.request(
            HttpMethod.POST, 
            `${this.model.getEndpoint()}/${id ? id : this.model.getPrimaryKeyValue()}/image`, 
            attributes,
            true,
            true,
        );
    }

    //------------------------ Basics ------------------------//

    /** 
     * HTTP request to the API
     * 
     * @param method HTTP method (GET, POST, PUT, PATCH, DELETE)
     * @param url API endpoint to call
     * @param payload The data to send
     * @param parseToModel Whether to parse the retrieved data to the model
     */
    public request(
        method: HttpMethod,
        url: string,
        payload: any = {},
        parseToModel = true,
        hasMedia = false,
    ): Promise<any> {
        const keys = Object.keys(this.$filters);
        if (keys.length) {
            keys.forEach(key => {
                const model: Model = (this.$filters as any)[key];
                if (model !== null && typeof model["getPrimaryKeyValue"] === "function") {
                    (this.$filters as any)[key] = model.getPrimaryKeyValue();
                }
            });
            payload.filters = this.$filters;
        }
        if (this.$page !== null) {
            payload.page = this.$page;
        }

        if (this.$limit !== null) {
            payload.limit = this.$limit;
        }

        if (this.$sort.key) {
            payload.sort =
                this.$sort.order === "DESC" ? `!${this.$sort.key}` : this.$sort.key;
        }

        if (this.$includes.length > 0) {
            payload.with = this.$includes;
        }

        return Client.request(method, url, payload, hasMedia).then(
            (response: Record<string, any>) => {
                return parseToModel
                    ? Promise.resolve(this.responseToModel(response))
                    : Promise.resolve(response);
            },
            (error: AxiosError) => {
                return Promise.reject(error);
            }
        );
    }

    /**
     * Converts retrieved data to the model.
     * 
     * @param response Response data to cast.
     */
    public responseToModel(response: any): any {
        const data = response.data;

        return data.items ?? data;
    }
}
