import Floor from "@/models/Floor";
import {Options, Vue} from "vue-class-component";
import {defaultTrend} from "@/components/charts/defaultTrend";
import {subjects} from "@/components/charts/subjects";
import Highcharts, { XrangePointOptionsObject } from "highcharts";
import FloorMap from "@/components/floormap/FloorMap.vue";
import HigherLowerIcon from '@/components/icons/HigherLowerIcon.vue';
import CheckmarkIcon from '@/components/icons/CheckmarkIcon.vue';
import { Filter } from "@/support/enums/Floor/Filter";
import { DataType } from "@/support/enums/Floor/DataType";
import Device from "@/models/Device";
import SensorData from "@/support/interfaces/SensorData";

@Options({
    components: {
        FloorMap,
        HigherLowerIcon,
        CheckmarkIcon,
    }
})
export default class FloorDetail extends Vue {
    protected isActive = false;
    protected floor: Floor | null = null;

    protected chart!: HTMLElement;
    protected options!: Highcharts.Options;
    protected subjects = subjects;
    protected active = 1;

    protected readonly filters: Array<DeviceFilter> = [
        { key: Filter.Year, text: "Jaar", },
        { key: Filter.Month, text: "Maand", },
        { key: Filter.Week, text: "Week", },
        { key: Filter.Day, text: "Dag", },
    ];
    protected currentFilter: DeviceFilter | null = null;
    protected toggleFilterSelection = false;

    protected readonly dataTypes: Array<DeviceDataType> = [
        { id: DataType.CO2, text: "CO2", },
        { id: DataType.CO, text: "CO", },
        { id: DataType.VOC, text: "VOC", },
        { id: DataType.PM2_5, text: "PM2.5", },
        { id: DataType.Temperature, text: "Temperatuur", },
        { id: DataType.Humidity, text: "Luchtvochtigheid", },
        { id: DataType.Battery, text: "Batterij", },
    ];
    protected currentDataType: DeviceDataType | null = null;
    protected toggleDataTypeSelection = false;

    protected graphData: Array<GraphData> = [];

    protected readonly lineColors: Array<string> = ['#f44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#03A9F4', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107', '#FF9800', '#FF5722'];

    public data() {
        return {
            floor: null,
        };
    }

    public mounted() {
        this.options = defaultTrend;
        this.chart = this.$refs.chart as HTMLElement;
        this.initialize();
    }

    protected async initialize() {
        if (!this.floor) {
            await this.getFloorFromUrl();
        }
        this.isActive = true;
    }

    protected async getFloorFromUrl() {
        const floorId = this.$route.params.floor as string;

        this.floor = await Floor
            .include(['building', 'devices'])
            .find(floorId)
            .catch(() => []);
    }

    protected async fillGraphData(filter: Filter): Promise<void> {
        if (this.floor == null) {
            return;
        }
        const floor = await Floor
            .include(['devices', `devices.${filter}`])
            .find(this.floor.id || '')
            .catch(() => null);

        if (floor == null || floor.devices == undefined) {
            return;
        } 
        this.graphData = [];

        floor.devices.forEach((device: Device) => {
            const data = {
                filter,
                device: device.id,
                co2: [],
                co: [],
                voc: [],
                pm2_5: [],
                temperature: [],
                humidity: [],
                created_at: [],
            } as GraphData;
            
            device.sensor_data.forEach((sensorData: SensorData) => {
                data.co2.push(sensorData.co2);
                data.co.push(sensorData.co);
                data.voc.push(sensorData.voc);
                data.pm2_5.push(sensorData.pm2_5);
                data.temperature.push(sensorData.temperature);
                data.humidity.push(sensorData.humidity);
                data.created_at.push(sensorData.created_at);
            });
            this.graphData.push(data);
        });
    }

    protected async changeActive(dataType: DataType, filter: Filter): Promise<void> {
        const subject = this.subjects.find(subject => subject.id === dataType);

        if (subject === undefined) {
            return;
        }

        await this.fillGraphData(filter);

        if (this.graphData == null) {
            return;
        }

        const series: Array<Highcharts.SeriesOptionsType> = [];

        let i = 0;
        this.graphData.forEach((data: GraphData) => {
            series.push({
                name: data.device,
                color: this.lineColors[i++],
                data: this.getDataSet(data, dataType),
                type: 'line',
            });
        });
        this.active = dataType;
        this.options.yAxis = subject.yAxis;
        this.options.series = series;
        if (this.options.plotOptions !== undefined && this.options.plotOptions.series !== undefined) {
            this.options.plotOptions.series.pointStart = 2020; // CONNECT THIS WITH INTERVAL LOGIC TO GET BOTTOM BAR CORRECT
        }
        Highcharts.chart(this.chart, this.options);
    }

    protected getDataSet(data: GraphData, dataType: DataType): Array<XrangePointOptionsObject> {
        switch (dataType) {
            case DataType.CO2: return data.co2 as Array<XrangePointOptionsObject>;
            case DataType.CO: return data.co as Array<XrangePointOptionsObject>;
            case DataType.VOC: return data.voc as Array<XrangePointOptionsObject>;
            case DataType.PM2_5: return data.pm2_5 as Array<XrangePointOptionsObject>;
            case DataType.Temperature: return data.temperature as Array<XrangePointOptionsObject>;
            case DataType.Humidity: return data.humidity as Array<XrangePointOptionsObject>;
            case DataType.Battery: return [];
            default: return [];
        }
    }

    protected setCurrentFilter(deviceFilter: DeviceFilter): void {
        this.currentFilter = (this.currentFilter != deviceFilter) ? deviceFilter : null;

        if (this.currentFilter != null && this.currentDataType != null) {
            this.changeActive(this.currentDataType.id, this.currentFilter.key);
        }
    }

    protected setCurrentDataType(deviceDataType: DeviceDataType): void {
        this.currentDataType = (this.currentDataType != deviceDataType) ? deviceDataType : null;

        if (this.currentFilter != null && this.currentDataType != null) {
            this.changeActive(this.currentDataType.id, this.currentFilter.key);
        }
    }
}

interface DeviceFilter {
    key: Filter;
    text: string;
}

interface DeviceDataType {
    id: DataType;
    text: string;
}

interface GraphData {
    filter: Filter;
    device: string;
    co2: Array<number|null>;
    co: Array<number|null>;
    voc: Array<number|null>;
    pm2_5: Array<number|null>;
    temperature: Array<number|null>;
    humidity: Array<number|null>;
    created_at: Array<Date|null>;
}
