import "./ResetPassword.scss";
import { Options, Vue } from "vue-class-component";
import Client from "@/support/Client";
import Axios from "axios";
import { HttpMethod } from "@/support/enums/HttpMethod";

@Options({
  components: {},
})
export default class ResetPassword extends Vue {
  email: string | null = null;
  token: string | null = null;
  password: string | null = null;

  beforeCreate(): void {
    if (typeof this.$route.query.token === "string") {
      this.token = this.$route.query.token;
    }
  }

  protected async resetPassword(): Promise<void> {
    if (this.token == null) {
      if (this.email != null) {
        await this.sendResetPassword({ email: this.email });
      }
      return;
    }

    if (this.password === null) {
      return;
    }
    await this.sendResetPassword({ password: this.password, token: this.token });
  }

  public async sendResetPassword({ password, token, email }: { password?: string, token?: string, email?: string }) {
    const baseURL = Client.getBaseUrl();
    const headers = { Accept: "application/json" };
    const client = Axios.create(({ baseURL, headers }));

    if (email) {
      return client[HttpMethod.GET](`users/reset_password/`, { params: { email } });
    }
    return client[HttpMethod.POST](`users/reset_password/${ token }`, { password });
  }
}
