import router from '@/router';
import Building from '@/models/Building';
import { Options, Vue } from 'vue-class-component';
import store from '@/store';
import { Role } from '@/support/enums/User/Role';
import DeleteIcon from '@/components/icons/DeleteIcon.vue';
import EditIcon from '@/components/icons/EditIcon.vue';
import SearchBar from '@/components/search/SearchBar.vue';
import { Modal, DeleteModal } from '@/components/modals';
import { ActiveModal } from '@/support/enums/ActiveModal';
import Client from '@/support/Client';
import { Grade } from '@/support/enums/Grade';

@Options({
  components: {
    DeleteIcon,
    EditIcon,
    SearchBar,
    Modal,
    DeleteModal,
  },
})
export default class BuildingOverview extends Vue {
  protected readonly adminMode = store.getters.user.role === Role.Admin.toLowerCase();

  protected buildings: Array<Building> = [];
  protected search = '';

  // Modal properties
  protected activeModal: ActiveModal = ActiveModal.None;
  protected selectedBuilding: Building | null = null;
  protected showErrors = false;

  // Properties
  protected buildingId = '';
  protected name = '';
  protected nameState = '';
  protected image: File | null = null;

  /*----------------- Modal Helpers -----------------*/

  /** Sets all modals to inactive. */
  protected hideModal(): void {
    this.activeModal = ActiveModal.None;

    this.clearModal();
  }

  /** Clears all modal values. */
  protected clearModal(): void {
    this.name = '';
    this.image = null;
    this.showErrors = false;
  }

  /**
   * Sets current active modal to given modal.
   * 
   * @param modal ActiveModal - Modal to use.
   * @param building Building - Building to use for the modal.
   */
  protected toggleModal(modal: ActiveModal, building: Building | null = null): void {
    this.activeModal = modal;
    this.selectedBuilding = building;

    if (building) {
      this.buildingId = building.id ?? '';
      this.name = building.name ?? '';
      this.nameState = building.name ?? '';
    }

    if (modal === ActiveModal.None) {
      this.clearModal();
    }
  }

  /** Creates a building with an API call using the values filled in on the Modal. */
  protected async createBuilding(): Promise<void> {
    this.showErrors = true;

    if (this.buildingAlreadyExists(this.name) || (this.image && !this.isValidImage(this.image))) {
      return;
    }

    let building = await Building.include('stats')
      .create({ name: this.name })
      .catch(() => null);

    if (!building) {
      return;
    }

    if (this.image) {
      building = await Building.image(building.id, { image: this.image }).catch(() => null);
      await this.getBuildings();
    } else {
      this.buildings.push(building);
    }

    this.hideModal();
  }

  /** Edits a building with an API call using the values filled in on the Modal. */
  protected async editBuilding(): Promise<void> {
    this.showErrors = true;

    if (!this.buildingId || !this.name) {
      return;
    }

    if (this.buildingAlreadyExists(this.name) || (this.image && !this.isValidImage(this.image))) {
      return;
    }

    let building = await Building.include('stats')
      .update(this.buildingId, {
        name: this.name,
      })
      .catch(() => null);

    if (this.image) {
      building = await Building.image(this.buildingId, { image: this.image }).catch(() => null);
      this.hideModal();
      await this.getBuildings();
    }

    if (building) {
      this.getBuildings();
      this.hideModal();
    }
  }

  /** Deletes a building using an API call, selected in the Delete Modal. */
  protected async deleteBuilding(): Promise<void> {
    if (!this.selectedBuilding) {
      return;
    }
    const index = this.buildings.findIndex((x) => x.id === this.selectedBuilding?.id);

    if (index > -1) {
      this.buildings.splice(index, 1);
    }

    await Building.delete(this.selectedBuilding.id ?? '').catch(() => null);

    this.hideModal();
  }

  /** Saves the image given in the image upload field into the upload variable to use when creating/updating the Building. */
  protected async handleFileUpload(): Promise<void> {
    const upload = this.$refs.upload as HTMLInputElement;
    const files: ReadonlyArray<File> = [...(upload.files ? upload.files : [])];
    this.image = files[0];
  }

  /*----------------- Page Logic -----------------*/

  /** Loads as soon as the page is loaded. */
  public async mounted() {
    this.getBuildings();
  }

  /**
   * Redirects page to given building.
   * 
   * @param building string - Building.id of the building.
   */
  protected selectBuilding(building: string): void {
    router.push({
      name: 'building',
      params: { building },
    });
  }

  /**
   * Reloads the buildings with an API call with the given search filter.
   *  
   * @param search string - String to filter buildings on.
   */
  protected async searchBuilding(search: string) {
    this.search = search;
    this.getBuildings();
  }

  /** Retrieves buildings from the API using a GET call. */
  protected async getBuildings(): Promise<void> {
    this.buildings = await Building.filter({ name: this.search })
      .include('stats')
      .list()
      .catch(() => []);
  }

  /**
   * Gets the URL of the building image.
   * 
   * @param building Building - Building to get the URL from.
   */
  protected getImage(building: Building): string {
    const baseURL = Client.getBaseUrl();
    const imageUrl = building._links.image ? `${baseURL}${building._links.image}` : '';

    return imageUrl;
  }

  /**
   * Gets building Grade from the score. Used for the colored orbs.
   * Warning - stats include is required for this to work.
   * 
   * @param building Building - Building to get the Grade from.
   */
  protected getGrade(building: Building): Grade {
    if ((building?.stats?.device_count ?? 0) < 1 || !building?.stats?.score) {
      return Grade.None;
    }

    const score = building.stats.score;
    if (score <= 40) {
      return Grade.Bad;
    }

    if (score <= 70) {
      return Grade.Average;
    }

    return Grade.Good;
  }

  /**
   * Checks whether a building with the same name already exists.
   * 
   * @param name string - Name to compare.
  */
  protected buildingAlreadyExists(name: string): boolean {
    const buildingAlreadyExists = this.buildings.find((building: Building) => building.name == name);

    if (this.activeModal == ActiveModal.Edit && this.nameState === name) {
      return false;
    }

    return buildingAlreadyExists ? true : false;
  }

  /**
   * Checks whether the given File is an image.
   * 
   * @param image File - File to check.
   */
  protected isValidImage(image: File): boolean {
    return image.type === 'image/jpeg' || image.type === 'image/jpg' || image.type === 'image/png';
  }
}
