import { Options, Vue } from 'vue-class-component';
import Building from '@/models/Building';
import router from '@/router';
import Floor from '@/models/Floor';
import { Modal, DeleteModal } from '@/components/modals';
import { ActiveModal } from '@/support/enums/ActiveModal';
import DeleteIcon from '@/components/icons/DeleteIcon.vue';
import EditIcon from '@/components/icons/EditIcon.vue';
import { Grade } from '@/support/enums/Grade';
import store from '@/store';
import { Role } from '@/support/enums/User/Role';

@Options({
  components: {
    DeleteIcon,
    EditIcon,
    Modal,
    DeleteModal,
  },
})
export default class BuildingDetail extends Vue {
  protected adminMode = store.getters.user.role === Role.Admin.toLowerCase();
  protected building: Building | null = null;

  // Modal properties
  protected activeModal: ActiveModal = ActiveModal.None;
  protected selectedFloor: Floor | null = null;

  // Properties
  protected level = '';
  protected image: File | null = null;

  /*----------------- Modal Helpers -----------------*/

  /** Sets all modals to inactive. */
  protected hideModal(): void {
    this.activeModal = ActiveModal.None;

    this.clearModal();
  }

  /** Clears all modal values. */
  protected clearModal(): void {
    this.level = '';
    this.image = null;
  }

  /**
  * Sets current active modal to given modal.
  *
  * @param modal ActiveModal - Modal to use.
  * @param floor Floor - Floor to use for the modal.
  */
  protected toggleModal(modal: ActiveModal, floor: Floor | null = null): void {
    this.activeModal = modal;
    this.selectedFloor = floor;

    if (modal === ActiveModal.None) {
      this.clearModal();
    }
  }

  /** Creates a floor with an API call using the values filled in on the Modal. */
  protected async createFloor(): Promise<void> {
    if (!this.building) {
      return;
    }

    const floor = await Floor.include('stats')
      .create({
        building_id: this.building.id,
        level: this.level,
      })
      .catch(() => null);

    if (!floor) {
      return;
    }
    this.building?.floors?.push(floor);

    if (this.image) {
      await Floor.image(floor.id, { image: this.image }).catch(() => null);
    }
    this.sortFloors();
    this.hideModal();
  }

  /** Edits a floor with an API call using the values filled in on the Modal. */
  protected async editFloor(): Promise<void> {
    if (!this.selectedFloor || !this.selectedFloor.id || this.selectedFloor.level == undefined) {
      return;
    }
    const floor = await Floor.include('stats')
      .update(this.selectedFloor.id, {
        level: this.selectedFloor.level,
      })
      .catch(() => null);

    if (this.image) {
      await Floor.image(this.selectedFloor.id, { image: this.image }).catch(() => null);
    }

    this.sortFloors();
    if (floor) {
      this.hideModal();
    }
  }

  /** Deletes a floor using an API call, selected in the Delete Modal. */
  protected async deleteFloor(): Promise<void> {
    if (!this.building || !this.building.floors || !this.selectedFloor) {
      return;
    }
    const index = this.building.floors.findIndex((x) => x.id === this.selectedFloor?.id);

    if (index > -1) {
      this.building.floors.splice(index, 1);
    }

    await Floor.delete(this.selectedFloor.id ?? '').catch(() => null);

    this.hideModal();
  }

  /** Saves the image given in the image upload field into the upload variable to use when creating/updating the Floor. */
  protected async handleFileUpload(): Promise<void> {
    const upload = this.$refs.upload as HTMLInputElement;
    const files: ReadonlyArray<File> = [...(upload.files ? upload.files : [])];
    this.image = files[0];
  }

  /** Sorts the floor array of the building on level, ascending order. */
  protected sortFloors(): void {
    if (!this.building || !this.building.floors) {
      return;
    }
    this.building.floors.sort((floor1, floor2) => (floor1.level ?? 0) - (floor2.level ?? 0));
  }

  /*----------------- Page Logic -----------------*/

  /** Loads as soon as the page is loaded. */
  public async mounted() {
    await this.refresh();
  }

  /** Refreshes page by retrieving all data again. */
  public async refresh(): Promise<void> {
    await this.getBuildingFromUrl();
  }

  /** Gets building ID from the route and retrieves the building with floors from the API. */
  protected async getBuildingFromUrl() {
    const buildingId = this.$route.params.building as string;

    this.building = await Building.include(['floors', 'floors.stats'])
      .find(buildingId || '')
      .catch(() => null);

    if (!this.building) {
      router.push({ name: 'buildings' });
      return;
    }
  }

  /**
   * Redirects page to given floor.
   *
   * @param floor string - Floor.id of the floor.
   */
  protected selectFloor(floor: string): void {
    router.push({
      name: 'floor',
      params: { floor },
    });
  }


  /**
   * Gets Floor Grade from the score. Used for the colored orbs.
   * Warning - stats include is required for this to work.
   *
   * @param floor Floor - Floor to get the Grade from.
   */
  protected getGrade(floor: Floor): Grade {
    if ((floor.stats?.device_count ?? 0) < 1 || !floor.stats?.score) {
      return Grade.None;
    }

    const score = floor.stats.score;
    if (score <= 40) {
      return Grade.Bad;
    }

    if (score <= 70) {
      return Grade.Average;
    }

    return Grade.Good;
  }
}
