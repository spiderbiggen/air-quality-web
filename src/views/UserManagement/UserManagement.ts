import { Options, Vue } from 'vue-class-component';
import store from '@/store';
import router from '@/router';
import User from '@/models/User';
import { Role } from '@/support/enums/User/Role';
import DeleteIcon from '@/components/icons/DeleteIcon.vue';
import EditIcon from '@/components/icons/EditIcon.vue';
import FilterIcon from '@/components/icons/FilterIcon.vue';
import HigherLowerIcon from '@/components/icons/HigherLowerIcon.vue';
import CheckmarkIcon from '@/components/icons/CheckmarkIcon.vue';
import SearchBar from '@/components/search/SearchBar.vue';
import { Modal, DeleteModal } from '@/components/modals';
import moment from 'moment';
import { convertDateToDutchFormat, convertDateToYearMonthDayFormat, isValidDateString } from '@/support/date/DateHelper';
import { capatalizeFirstLetter } from '@/support/WordFormatter';
import { isEmailValid, isPasswordValid } from '@/support/user/UserValidationHelper';

@Options({
  components: {
    DeleteIcon,
    EditIcon,
    FilterIcon,
    HigherLowerIcon,
    CheckmarkIcon,
    SearchBar,
    Modal,
    DeleteModal,
  },
  methods: {
    convertDateToDutchFormat,
    convertDateToYearMonthDayFormat,
    capatalizeFirstLetter,
    isValidDateString,
    isEmailValid,
    isPasswordValid,
  },
})
export default class UserManagement extends Vue {
  protected users: Array<User> = [];
  protected roles: Array<string> = [];
  protected searchString = '';
  protected toggleRoleSelection = false;
  protected isCreateModalVisible = false;
  protected isEditModalVisible = false;
  protected isFilterActive = false;

  protected isDeleteModalVisible = false;
  protected userToBeDeleted: User | undefined = undefined;
  protected loggedInUser = store.getters.user;

  protected userId = '';
  protected email = '';
  protected emailState = '';
  protected password = '';
  protected filteredRole: Role | null = null;
  protected currentRole: Role | string | null = null;
  protected repeatPassword = null;
  protected disabledAt = '';
  protected showErrors = false;
  protected disabledAtPlaceholder = moment()
    .add(2, 'years')
    .format('YYYY-MM-DD');

  public mounted() {
    if (store.getters.user.role !== Role.Admin.toLowerCase()) {
      router.push({ name: 'buildings' });
    }
    this.getUsers();
    this.getRoles();
  }

  protected getRoles() {
    this.roles = [];

    for (const value of this.enumKeys(Role)) {
      this.roles.push(Role[value]);
    }
  }

  protected clearModal(): void {
    this.email = '';
    this.password = '';
    this.currentRole = null;
    this.repeatPassword = null;
    this.disabledAt = '';
    this.showErrors = false;

    this.userToBeDeleted = undefined;
  }

  protected toggleFilter(): void {
    this.isFilterActive = !this.isFilterActive;
  }

  protected toggleCreateModal(state: boolean): void {
    this.isCreateModalVisible = state;

    if (!state) {
      this.clearModal();
    }
  }

  protected toggleEditModal(state: boolean, user?: User): void {
    this.toggleRoleSelection = false;
    this.isEditModalVisible = state;

    if (user) {
      this.userId = user.id!;
      this.email = user.email!;
      this.emailState = user.email!;
      this.currentRole = capatalizeFirstLetter(user.role!);
    }

    if (user?.disabled_at) {
      this.disabledAt = convertDateToYearMonthDayFormat(new Date(user.disabled_at!));
    }
  }

  protected toggleDeleteModal(state: boolean, user?: User): void {
    this.isDeleteModalVisible = state;
    this.userToBeDeleted = user;

    if (!state) {
      this.clearModal();
    }
  }

  protected hideModal(): void {
    this.isCreateModalVisible = false;
    this.isEditModalVisible = false;

    this.clearModal();
  }

  protected setCurrentRole(role: Role): void {
    this.currentRole = role;
  }

  protected enumKeys<O extends object, K extends keyof O = keyof O>(obj: O): Array<K> {
    return Object.keys(obj).filter((k) => Number.isNaN(+k)) as K[];
  }

  protected emailAlreadyExists(email: string): boolean {
    const userAlreadyExists = this.users.find((user: User) => user.email == email);

    if (this.isEditModalVisible && this.emailState === email) {
      return false;
    }

    return userAlreadyExists ? true : false;
  }

  protected isActiveUser(user: User): boolean {
    return user.disabled_at == null || new Date() < new Date(user.disabled_at);
  }

  protected async createUser() {
    this.showErrors = true;

    if (
      !isEmailValid(this.email) ||
      this.emailAlreadyExists(this.email) ||
      !isPasswordValid(this.password) ||
      this.password !== this.repeatPassword ||
      !this.currentRole
    ) {
      return;
    }

    if (this.disabledAt) {
      if (!isValidDateString(this.disabledAt)) {
        return;
      }
    }

    const user = await User.create({
      email: this.email,
      password: this.password,
      role: this.currentRole,
      disabled_at: this.disabledAt && moment(this.disabledAt).format('YYYY-MM-DD'),
    }).catch(() => null);

    if (user) {
      this.users.push(user);
      this.toggleCreateModal(false);
      this.clearModal();
    }
  }

  protected async editUser() {
    this.showErrors = true;

    if (!this.userId || !this.email) {
      return;
    }

    if (!isEmailValid(this.email) || this.emailAlreadyExists(this.email) || !this.currentRole) {
      return;
    }

    if (this.disabledAt) {
      if (!isValidDateString(this.disabledAt)) {
        return;
      }
    }

    const user = await User.update(this.userId, {
      email: this.email,
      role: this.currentRole,
      disabled_at: this.disabledAt ? moment(this.disabledAt).format('YYYY-MM-DD') : null,
    }).catch(() => null);

    if (user) {
      this.getUsers();
      this.toggleEditModal(false);
      this.clearModal();
    }
  }

  protected async searchUser(searchString: string) {
    this.searchString = searchString;
    this.getUsers();
  }

  protected async getUsersWithRole(role: Role) {
    if (this.filteredRole === role) {
      this.filteredRole = null;
    } else {
      this.filteredRole = role;
    }

    this.getUsers();
  }

  protected async toggleActiveUser(user: User): Promise<void> {
    const updatedUser = await User.update(user.id ?? '', {
      disabled_at: this.isActiveUser(user)
        ? moment(new Date()).format('YYYY-MM-DD')
        : moment(new Date())
            .add(1, 'years')
            .format('YYYY-MM-DD'),
    }).catch(() => null);
    user.disabled_at = updatedUser.disabled_at;
  }

  protected async getUsers(): Promise<void> {
    this.users = await User.filter({ email: this.searchString, role: this.filteredRole })
      .list()
      .catch(() => []);
  }

  protected async deleteUser(): Promise<void> {
    if (!this.userToBeDeleted) {
      return;
    }
    const index = this.users.findIndex((x) => x.id === this.userToBeDeleted?.id);

    if (index > -1) {
      this.users.splice(index, 1);
    }

    await User.delete(this.userToBeDeleted.id ?? '').catch(() => null);

    this.toggleDeleteModal(false);
  }
}
