import "./Activate.scss";
import { Options, Vue } from "vue-class-component";
import Client from "@/support/Client";
import Axios, { AxiosStatic } from "axios";
import { HttpMethod } from "@/support/enums/HttpMethod";

@Options({
  components: {},
})
export default class Activate extends Vue {
  password: string | null = null;
  token: string | null = null;

  beforeCreate(): void {
    if (typeof this.$route.query.token === "string") {
      this.token = this.$route.query.token;
    }
  }


  protected async activate(): Promise<void> {
    if (!this.password || !this.token) {
      return;
    }
    await this.sendActivate({ token: this.token, password: this.password });
  }

  async sendActivate({ token, password }: { token: string, password: string }) {
    const baseURL = Client.getBaseUrl();
    const headers = { Accept: "application/json" };
    const client = Axios.create(({ baseURL, headers }));

    return (client as AxiosStatic)[HttpMethod.POST](`users/activate/${ token }`, { password });
  }
}
