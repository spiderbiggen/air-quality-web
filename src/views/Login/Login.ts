import "./Login.scss";
import { Options, Vue } from "vue-class-component";
import router from "@/router";
import store from "@/store";
import Client from "@/support/Client";
import User from "@/models/User";

@Options({
  components: {}
})
export default class Login extends Vue {
  email: string | null = null;
  password: string | null = null;
  isChecked = false;

  public async mounted() {
    this.email = store.state.login.email;
    this.isChecked = store.state.login.isChecked;
  }

  protected async login(): Promise<void> {
    if (this.email === null || this.password === null) {
      return;
    }
    const response = await Client.login({ email: this.email, password: this.password});
    const token = response?.data?.access_token;

    if (! token) {
      return;
    }
    store.dispatch('tokenUpdate', `Bearer ${token}`);

    const user = await User.current() as User;
    store.dispatch('userUpdate', user);

    store.dispatch('loginInfoUpdate', {
      email: this.isChecked ? this.email : '',
      isChecked: this.isChecked,
    });

    router.push({ name: "buildings" });
  }
}
