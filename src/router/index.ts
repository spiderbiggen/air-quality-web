import store from '@/store';
import { createRouter, createWebHistory, RouteRecordRaw, } from 'vue-router';

function isAuthenticated(): boolean {
  return store.getters.token !== null;
}

function authenticationGuard() {
  return isAuthenticated() ? true : {name: 'login'}
}

const publicPath = process.env.VUE_APP_PUBLIC_PATH
const routes: Array<RouteRecordRaw> = [
  { path: "/", name: "home", redirect: "buildings", },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login/Login.vue")
  },
  {
    path: "/activate",
    name: "activate",
    component: () =>
      import(/* webpackChunkName: "activate" */ "../views/Activate/Activate.vue")
  },
  {
    path: "/reset_password",
    name: "reset-password",
    component: () =>
      import(/* webpackChunkName: "reset-password" */ "../views/ResetPassword/ResetPassword.vue")
  },
  {
    path: "/buildings",
    name: "buildings",
    component: () =>
      import(/* webpackChunkName: "buildings" */ "../views/Building/BuildingOverview.vue"),
    beforeEnter: [authenticationGuard],
  },
  {
    path: "/buildings/:building",
    name: "building",
    component: () =>
      import(/* webpackChunkName: "building" */ "../views/Building/BuildingDetail.vue" ),
    beforeEnter: [authenticationGuard],
  },
  {
    path: "/floors/:floor",
    name: "floor",
    component: () =>
      import(/* webpackChunkName: "floor" */ "../views/Floor/FloorDetail.vue" ),
    beforeEnter: [authenticationGuard],
  },
  {
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About/About.vue"),
    beforeEnter: [authenticationGuard],
  },
  {
    path: "/user-management",
    name: "user-management",
    component: () =>
      import(/* webpackChunkName: "user-mangement" */ "../views/UserManagement/UserManagement.vue" ),
    beforeEnter: [authenticationGuard],
  },
];

const router = createRouter({
  history: createWebHistory(publicPath),
  routes
});

export default router;
