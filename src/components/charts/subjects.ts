import { DataType } from "@/support/enums/Floor/DataType";

export const subjects = [
  {
    id: DataType.CO2,
    name: "CO2",
    color: "#673AB7",
    yAxis: {
      title: { text: "% (Percent)" },
      plotBands: [
        { from: 0, to: 30, color: "#2196F325" },
        { from: 30, to: 75, color: "#FFEB3B25" },
        { from: 75, to: 100, color: "#FF980025" }
      ]
    },
  },
  {
    id: DataType.CO,
    name: "CO",
    color: "#4CAF50",
    yAxis: {
      title: { text: "% (Percent)" },
      plotBands: [
        { from: 0, to: 30, color: "#2196F325" },
        { from: 30, to: 75, color: "#FFEB3B25" },
        { from: 75, to: 100, color: "#FF980025" }
      ]
    },
  },
  {
    id: DataType.VOC,
    name: "VOC",
    color: "#f44336",
    yAxis: {
      title: { text: "PPM" },
      plotBands: [
        { from: 0, to: 10, color: "#2196F325" },
        { from: 10, to: 20, color: "#FFEB3B25" },
        { from: 20, to: 30, color: "#FF980025" }
      ]
    },
  },
  {
    id: DataType.PM2_5,
    name: "PM2.5",
    color: "#2196F3",
    yAxis: {
      title: { text: "PPM" },
      plotBands: [
        { from: 0, to: 10, color: "#2196F325" },
        { from: 10, to: 20, color: "#FFEB3B25" },
        { from: 20, to: 30, color: "#FF980025" }
      ]
    },
  },
  {
    id: DataType.Temperature,
    name: "Temperatuur",
    color: "#7E57C2",
    yAxis: {
      title: { text: "°C (Degrees Celsius)" }
    },
  },
  {
    id: DataType.Humidity,
    name: "Luchtvochtigheid",
    color: "#f44336",
    yAxis: {
      title: { text: "% (Percent)" },
      plotBands: [
        { from: 0, to: 30, color: "#2196F325" },
        { from: 30, to: 75, color: "#FFEB3B25" },
        { from: 75, to: 100, color: "#FF980025" }
      ]
    },
  },
  {
    id: DataType.Battery,
    name: "Batterij",
    color: "#f44336",
    yAxis: {
      title: { text: "% (Percent)" }
    },
  }
];
