import Highcharts from "highcharts";

export const defaultTrend = {
  title: {
    text: ""
  },
  yAxis: {
    title: {
      text: "% (Percent)"
    },
    plotBands: []
  },
  xAxis: {
    accessibility: {
      rangeDescription: ""
    }
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle"
  },
  plotOptions: {
    series: {
      label: {
        connectorAllowed: false
      },
      pointStart: 2010
    }
  },
  series: [],
  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: "horizontal",
            align: "center",
            verticalAlign: "bottom"
          }
        }
      }
    ]
  }
} as Highcharts.Options;
