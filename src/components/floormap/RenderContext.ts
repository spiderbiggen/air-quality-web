import Hammer from "hammerjs";
import Device from "@/models/Device";

const baseURL = process.env.VUE_APP_API_URL ?? "https://localhost:8080";
export const DEVICE_SIZE = 10;

type Optional<T> = T | null

const HAMMER_OPTIONS: HammerOptions = {
    recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}],
        [Hammer.Pinch, {enable: true}],
        [Hammer.Tap]
    ]
};

export type RenderContextOptions = {
    minZoom?: number;
    maxZoom?: number;
    zoom?: number;
}

export type PositionedDevice = Partial<Device> & {x?: number, y?: number}
export type TapCallback = (ev: PositionedDevice) => void;

export class RenderContext {
    public minZoom = 0;
    public maxZoom = 2;

    private readonly tapCallback?: TapCallback;
    private readonly canvas: HTMLCanvasElement;
    private readonly context: Optional<CanvasRenderingContext2D>;
    private readonly hammer: HammerManager;
    private image: Optional<HTMLImageElement> = null;
    private devices: Device[] = [];
    private xOffset = 0;
    private yOffset = 0;
    private mapWidth = 0;
    private mapHeight = 0;
    private drawWidth = 0;
    private drawHeight = 0;
    private zoom = 0;
    private lastPan = 0;
    private lastScale = 0;

    constructor(canvas: HTMLCanvasElement, tapCallback?: TapCallback, options: RenderContextOptions = {}) {
        this.canvas = canvas;
        this.tapCallback = tapCallback;
        this.context = canvas.getContext('2d');
        this.hammer = new Hammer(this.canvas, HAMMER_OPTIONS);
        this.minZoom = options.minZoom || this.minZoom;
        this.maxZoom = options.maxZoom || this.maxZoom;
        this.zoom = options.zoom || this.zoom;
    }

    get canvasWidth() {
        return this.canvas.width;
    }

    get canvasHeight() {
        return this.canvas.height;
    }

    private get naturalWidth() {
        return this.image?.naturalWidth;
    }

    private get naturalHeight() {
        return this.image?.naturalHeight;
    }

    public initializeListeners() {
        window.addEventListener("resize", this.resize.bind(this));
        this.canvas.addEventListener("wheel", this.wheelZoom.bind(this));
        this.hammer.on('pan panstart pinch pinchend pinchstart', this.panPinch.bind(this));
        this.hammer.on('tap', this.tap.bind(this));
    }

    public destroyListeners() {
        window.removeEventListener("resize", this.resize);
    }

    private async tap(ev: HammerInput) {
        const tap = ev.srcEvent as PointerEvent;
        const devices = this.mapDevices();
        const deviceRadiusSquared = DEVICE_SIZE ** 2;
        const device = devices.find(device => (device.x - tap.offsetX) ** 2 + (device.y - tap.offsetY) ** 2 < deviceRadiusSquared);
        if (device && this.tapCallback) {
            this.tapCallback(device);
        }
    }

    async resize() {
        const rect = this.canvas.parentElement?.getBoundingClientRect();
        if (!rect) return;
        this.canvas.width = rect.width;
        this.canvas.height = rect.height;
        this.sizeImage()
        this.changeZoom(0);
        await this.draw();
    }

    private async wheelZoom(e: WheelEvent) {
        this.changeZoom(-e.deltaY * 0.001, e.offsetX, e.offsetY);
        await this.draw();
        e.preventDefault();
    }

    draw() {
        if (!this.context || !this.image) return;
        if (this.drawWidth < this.canvasWidth || this.drawHeight < this.canvasHeight) {
            this.context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        }
        this.context.drawImage(this.image, this.xOffset, this.yOffset, this.drawWidth, this.drawHeight);
        if (this.devices.length > 0) {
            this.context.save();
            this.context.beginPath();
            const devices = this.mapDevices();
            for (const device of devices) {
                if (!device.x) continue;
                this.context.moveTo(device.x, device.y);
                this.context.fillStyle = "#9f9f9f"; // TODO set color
                this.context.arc(device.x, device.y, DEVICE_SIZE, 0, 2 * Math.PI);
            }
            this.context.closePath();
            this.context.fill();
            this.context.restore();
        }
    }

    mapDevices(): Array<Partial<Device> & { x: number, y: number }> {
        return this.devices.filter(device => device.x_position != null && device.y_position != null)
            .map(device => {
                return {
                    ...device,
                    x: (device.x_position || 0) * this.drawWidth + this.xOffset,
                    y: (device.y_position || 0) * this.drawHeight + this.yOffset,
                };
            });
    }


    private setX(newX: number) {
        const minX = this.canvasWidth - this.drawWidth;
        if (minX < 0) {
            this.xOffset = Math.max(minX, Math.min(0, newX));
        } else {
            this.xOffset = Math.max(0, Math.min(minX, newX));
        }
    }

    private setY(newY: number) {
        const minY = this.canvasHeight - this.drawHeight;
        if (minY < 0) {
            this.yOffset = Math.max(minY, Math.min(0, newY));
        } else {
            this.yOffset = Math.max(0, Math.min(minY, newY));
        }
    }

    private recalculateDrawSize() {
        const zoom = Math.pow(2, this.zoom);
        this.drawWidth = this.mapWidth * zoom;
        this.drawHeight = this.mapHeight * zoom;
    }

    private changeZoom(dZoom: number, centerX: number = this.canvasWidth / 2, centerY: number = this.canvasHeight / 2) {
        //between 0-1
        const imageX = (centerX - this.xOffset) / this.drawWidth;
        const imageY = (centerY - this.yOffset) / this.drawHeight;

        this.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.zoom + dZoom));

        this.recalculateDrawSize();
        this.setX(centerX - (imageX * this.drawWidth));
        this.setY(centerY - (imageY * this.drawHeight));
    }

    async setBackground(imagePath: string) {
        if (!imagePath) return;
        const url = baseURL + imagePath;
        if (this.image?.src === url) return;

        const image = new Image();
        image.src = url;
        await new Promise(resolve => {
            image.onload = resolve;
        });
        this.image = image;
        this.sizeImage();
        this.draw();
    }

    setDevices(devices: Device[]) {
        this.devices = devices || [];
        this.draw();
    }

    private sizeImage() {
        if (!this.image) return;
        const scalar = this.canvasWidth / this.image.naturalWidth;
        const zoom = Math.pow(2, this.zoom);
        this.mapWidth = this.image.naturalWidth * scalar;
        this.mapHeight = this.image.naturalHeight * scalar;
        this.drawWidth = this.mapWidth * zoom;
        this.drawHeight = this.mapHeight * zoom;
        this.setX((this.canvasWidth - this.drawWidth) / 2);
        this.setY((this.canvasHeight - this.drawHeight) / 2);
    }

    async panPinch(ev: HammerInput) {
        if (ev.type == 'panstart' || ev.type === 'pinchstart') {
            this.lastPan = ev.deltaTime;
            if (ev.type === 'pinchstart') {
                this.lastScale = ev.scale;
            }
        }
        if (ev.type === 'pan' || ev.type === 'pinch') {
            const deltaTime = ev.deltaTime - this.lastPan;
            this.setX(this.xOffset + ev.velocityX * deltaTime);
            this.setY(this.yOffset + ev.velocityY * deltaTime);
            this.lastPan = ev.deltaTime;
        }
        if (ev.type === 'pinch') {
            this.changeZoom(ev.scale - this.lastScale);
            this.lastScale = ev.scale;
        }
        this.draw();
    }
}
