import Device from "@/models/Device";
import Floor from "@/models/Floor";
import {Options, Vue} from "vue-class-component";
import {PropType} from "vue";
import {PositionedDevice, RenderContext} from "@/components/floormap/RenderContext";
import DevicePopup from "@/components/floormap/popup/DevicePopup.vue";


@Options({
  props: {
    floor: {
      type: Object as PropType<Floor>,
    },
    devices: {
      type: Array as PropType<Array<Device>>,
      required: true,
    },
    minZoom: {
      type: Number,
    },
    maxZoom: {
      type: Number,
      default: 2,
    },
    editMode: {
      type: Boolean,
      default: false,
    }
  },
  data() {
    return {
      context: null,
      activeDevice: null,
    }
  },
  watch: {
    floor() {
      this.setBackground();
    },
    devices: {
      immediate: true,
      handler() {
        this.setDevices();
      }
    }
  },
  components: {
    DevicePopup,
  }
})
export default class FloorMap extends Vue {
  readonly floor ?: Floor;
  readonly devices !: Device[];

  private context?: RenderContext;
  private activeDevice?: PositionedDevice;

  async mounted(): Promise<void> {
    this.context = new RenderContext(this.$refs.canvas as HTMLCanvasElement, this.deviceCallback);
    this.context?.initializeListeners();
    this.context?.resize()
    this.setBackground();
  }

  public unmounted(): void {
    this.context?.destroyListeners();
  }

  public beforeUpdate() {
    this.context?.draw();
  }

  protected setBackground() {
    if (this.floor) {
      this.context?.setBackground(this.floor._links.image);
    }
  }

  protected setDevices() {
    if (this.devices) {
      this.context?.setDevices(this.devices);
    }
  }

  protected clearDevice() {
    this.activeDevice = undefined;
  }

  protected deviceCallback(device: PositionedDevice) {
    this.activeDevice = device;
  }

  protected closePopup() {
    this.activeDevice = undefined;
  }
}
