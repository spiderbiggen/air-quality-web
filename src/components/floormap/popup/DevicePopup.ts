import Device from "@/models/Device";
import {Options, Vue} from "vue-class-component";
import {PropType} from "vue";
import "./DevicePopup.scss";
import Modal from "@/components/modals/Modal.vue";
import {DEVICE_SIZE} from "@/components/floormap/RenderContext";

@Options({
    props: {
        device: {
            type: Object as PropType<Device>,
        },
        x: {
            type: Number,
            required: true,
        },
        y: {
            type: Number,
            required: true,
        },
    },
    data() {
        return {
            yOffset: 0,
            xOffset: 0,
            arrowClass: "bottom",
        };
    },
    emits: ["close"],
    components: {
        Modal,
    }
})
export default class DevicePopup extends Vue {
    private device?: Device;
    private x!: number;
    private y!: number;

    private xOffset!: number;
    private yOffset!: number;
    private arrowClass!: "bottom" | "top" | "left" | "right" | "none";

    mounted(): void {
        this.setPosition();
    }

    beforeUpdate(): void {
        this.setPosition();
    }

    setPosition() {
        const {xOffset, yOffset, deltaX, deltaY} = this.calculateOffsets();
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        if (deltaX != 0 && deltaY != 0) {
            this.arrowClass = "none";
        } else if (deltaX > 0) {
            this.arrowClass = "left";
        } else if (deltaX < 0) {
            this.arrowClass = "right";
        } else if (deltaY > 0) {
            this.arrowClass = "top";
        } else if (deltaY < 0) {
            this.arrowClass = "bottom";
        }
    }

    calculateOffsets(): { xOffset: number, yOffset: number, deltaX: number, deltaY: number } {
        const popupEl = this.$refs.popup as HTMLDivElement;
        const rect = popupEl.parentElement?.getBoundingClientRect();
        if (!rect) throw new Error("can't happen");


        const popupWidth = popupEl.clientWidth;
        const popupHeight = popupEl.clientHeight;
        const halfWidth = popupWidth / 2;
        const halfHeight = popupHeight / 2;

        if (popupWidth * 2 > rect.width) {
            return {
                xOffset: (rect.width / 2) - halfWidth,
                yOffset: (rect.height / 2) - halfHeight,
                deltaX: NaN,
                deltaY: NaN,
            };
        }

        const result = {
            xOffset: this.x - halfWidth,
            yOffset: this.y,
            deltaX: 0,
            deltaY: 1,
        };
        if (result.xOffset - halfWidth < 0) {
            result.deltaX = 1;
            result.deltaY = 0;
            result.xOffset = this.x;
            result.yOffset = this.y - halfHeight;
        } else if (result.xOffset + popupWidth > rect.width) {
            result.deltaX = -1;
            result.deltaY = 0;
            result.xOffset = this.x - popupWidth;
            result.yOffset = this.y - halfHeight;
        }
        if (result.yOffset - halfHeight < 0) {
            result.deltaY = 1;
            result.xOffset =
            result.yOffset = this.y;
        } else if (result.yOffset + popupHeight > rect.height) {
            result.deltaY = -1;
            result.yOffset = this.y - popupHeight;
        }
        result.xOffset += result.deltaX * (20 + DEVICE_SIZE);
        result.yOffset += result.deltaY * (20 + DEVICE_SIZE);
        return result;
    }
}
