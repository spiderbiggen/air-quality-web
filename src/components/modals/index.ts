import Modal from './Modal.vue';
import DeleteModal from './DeleteModal.vue';

export { Modal, DeleteModal };
