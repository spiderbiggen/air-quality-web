```
           _         ____              _ _ _          __          __  _     
     /\   (_)       / __ \            | (_) |         \ \        / / | |    
    /  \   _ _ __  | |  | |_   _  __ _| |_| |_ _   _   \ \  /\  / /__| |__  
   / /\ \ | | '__| | |  | | | | |/ _` | | | __| | | |   \ \/  \/ / _ \ '_ \ 
  / ____ \| | |    | |__| | |_| | (_| | | | |_| |_| |    \  /\  /  __/ |_) |
 /_/    \_\_|_|     \___\_\\__,_|\__,_|_|_|\__|\__, |     \/  \/ \___|_.__/ 
                                                __/ |                       
                                               |___/                        
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
